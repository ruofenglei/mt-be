FROM golang:1.12 as build
WORKDIR /code
COPY go.mod go.sum /code/
RUN GOPROXY=https://goproxy.io go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64  go build -o /go/bin/app

FROM alpine
COPY --from=build /go/bin/app /go/bin/app
EXPOSE 3000
ENTRYPOINT ["/go/bin/app"]