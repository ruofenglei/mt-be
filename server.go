package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 30 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = 25 * time.Second

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

// Client is an middleman between the websocket connection and the hub.
type Client struct {
	// 每个client实例中都有一个指向hub的指针
	hub *Hub
	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

//Websocket握手配置
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type Message struct {
	EventType string          `json:"event"`
	X         json.RawMessage `json:"data"`
}

//RequestMessage 请求消息结构体
type RequestMessage struct {
	Msg string `json:"msg"`
}

//ResponseMessage 响应消息结构体
type ResponseMessage struct {
	Msg string `json:"msg"`
}

type RedirectMessage struct {
	Target string `json:"target"`
	From   string `json:"from"`
	To     string `json:"to"`
}

//Websocket链接池
var clients = make(map[*websocket.Conn]bool)

//BroadCast Channel
var broadcast = make(chan ResponseMessage)

func (c *Client) readPump() {
	defer func() {
		log.Println("Close and unregister client")
		log.Print(c.hub.clients)
		c.hub.unregister <- c
		c.conn.Close()
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	// 读取browser传来的ws信息
	for {
		_, message, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("error: %v", err)
			}
			// 无法从conn读消息，直接跳到defer关闭这个链接
			break
		}
		// 直接广播收到的信息
		//message = bytes.TrimSpace(bytes.Replace(message, newline, space, -1))
		var msg Message
		err = json.Unmarshal(message, &msg)
		if err != nil {
			log.Printf("error: %v", err)
			continue
		}
		if msg.EventType == "redirect" && msg.X != nil {
			c.hub.broadcast <- message
		}
	}
}

func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop() // todo: ticker需要停止？
		c.conn.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			w, err := c.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(newline)
				w.Write(<-c.send)
			}
			if err := w.Close(); err != nil {
				return
			}

		case <-ticker.C:
			// 设置写入ping消息的ddl
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				// ping失败
				// 1. ddl到了但是都没来得及向conn写入ping消息，表明buffer阻塞
				// 2. 已经ping，但是未收到pong，表明客户端断开了链接
				// 则直接到defer 停止ticker计时并关闭ws链接
				return
			}
		}
	}
}

func serveWs(hub *Hub, w http.ResponseWriter, r *http.Request) {
	// ws握手
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	// 创建新链接
	log.Println("New Client")
	client := &Client{
		hub:  hub,
		conn: conn,
		send: make(chan []byte, 256),
	}
	// 注册新链接到链接池
	client.hub.register <- client

	go client.readPump()
	go client.writePump()
}

func main() {

	//hub 用于维护所有的Websocket链接，以及broadcast
	hub := newHub()
	go hub.run()

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(hub, w, r)
	})
	log.Println("http server started on :3000")

	err := http.ListenAndServe("0.0.0.0:3000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
